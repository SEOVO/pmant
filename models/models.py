# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.http import request
#from odoo.addons.ehcs_qr_code_base.qr_code_base import generate_qr_code 

import qrcode
import base64
from io import BytesIO
#import cStringIO

def generate_qr_code(url):
    qr = qrcode.QRCode(
             version=1,
             error_correction=qrcode.constants.ERROR_CORRECT_L,
             box_size=20,
             border=4,
             )
    qr.add_data(url)
    qr.make(fit=True)
    img = qr.make_image()
    temp = BytesIO()
    img.save(temp, format="PNG")
    qr_img = base64.b64encode(temp.getvalue())
    return qr_img

class Ubicaciones(models.Model):
      _name        = 'ubicaciones.mantenimiento'
      name         = fields.Char(required=True)
      direccion    = fields.Char(required=True)
      ciudad       = fields.Char()
      departamento = fields.Char()
      cod_area     = fields.Char()
      pais         = fields.Char()
      telefono     = fields.Char()
      comentario   = fields.Char()

class Prioridad(models.Model):
     _name        = 'prioridad.mantenimiento'
     name         = fields.Char(required=True)
class Equipo(models.Model):
    _name        = 'maintenance.equipment'
    _inherit     = 'maintenance.equipment'
    ubicacion    = fields.Many2one('ubicaciones.mantenimiento',string="Ubicaciones",required=True)
    fabricante   = fields.Char(size=60)
    fecha_compra = fields.Date()
    otro1        = fields.Char(size=50,string="Clasificacion 1")
    otro2        = fields.Char(size=50, string = "Clasificacion 2")
    time_uso     = fields.Float(String="Horas Promedio")
    prioridad    = fields.Many2one('prioridad.mantenimiento',string="Prioridad")
    adjunto      = fields.One2many('adjunto.mantenimiento','equipo',string="Archivos Adjuntos")
    planequipo   = fields.One2many('planequipo.mantenimiento','equipo',string="Planes Equipos")
    qr_image     = fields.Binary("QR Code", compute='_generate_qr_code')
    qr_image2    = fields.Binary("QR Code", compute='_generate_qr_code')
    url_qr       = fields.Char(string='URL del QR',compute='_generate_qr_code')
    @api.one
    def _generate_qr_code(self):
        #base_url = request.env['ir.config_parameter'].get_param('web.base.url')
        base_url = 'http://peru2019.ddns.net:8075/'
        base_url += '/web#id=%d&view_type=form&model=%s' % (self.id,self._name) 
        self.qr_image = generate_qr_code(base_url)
        self.qr_image2 = self.qr_image
        self.url_qr   = base_url

class Adjunto(models.Model):
    _name        = 'adjunto.mantenimiento'
    name         = fields.Char(size=60,string='Referencia Archivo')
    adjunto      = fields.Binary()
    equipo       = fields.Many2one('maintenance.equipment',string='Equipo')
    
class Plan(models.Model):
    _name  = 'plan.mantenimiento'
    name         = fields.Char(size=60,required=True,string='Nombre')
    frecuencia   = fields.Integer(required=True,string="Unidad Frecuencia")
    tipo         = fields.Many2one('tipoplan.mantenimiento',string="Tipo Frecuencia")
    proceso      = fields.One2many('proceso.mantenimiento','plan',string="procesos")
    descripcion  = fields.Text()

class PlanEquipoProcesos(models.Model):
    _name        = 'planequipoproceso.mantenimiento'
    proceso      = fields.Many2one('proceso.mantenimiento',string="Proceso", required=True)
    planequipo   = fields.Many2one('planequipo.mantenimiento',string='Plan Equipo')
    descripcion  = fields.Text()
    estado       = fields.Many2one('estadoproceso.mantenimiento',string="Estado")
    tarea        = fields.Many2one('tarea.mantenimiento',string="Tarea")
    ots          = fields.Many2one('maintenance.request',string="ots")
    plan         = fields.Many2one('plan.mantenimiento',related="planequipo.plan")

class PlanEquipo(models.Model):
    _name        = 'planequipo.mantenimiento'
#    name         = fields.Char('PLAN-EQUIPO',compute='_generate_name')
    plan         = fields.Many2one('plan.mantenimiento',string='Plan de Tarea',required=True)
    equipo       = fields.Many2one('maintenance.equipment',string='Equipo',required=True) 
    ubicacion    = fields.Many2one('ubicaciones.mantenimiento',string='Ubicacion',required=True)
    tarea        = fields.Many2one('tarea.mantenimiento',string='Tarea',required=True)  
    procesos     = fields.One2many('planequipoproceso.mantenimiento','planequipo' , string="Procesos a Utilizar")
    @api.multi 
    @api.onchange('plan')
    def _onchange_procesos(self):
        self.procesos  = False
        for record in self.plan.proceso:
            self.procesos += self.env['planequipoproceso.mantenimiento'].new({'proceso': record.id,'planequipo':self.id,'tarea':self.tarea,'plan':self.plan})
#    @api.one
#    def _generate_name(self):
#        self.name = str(self.plan.name) + ' / '+str(self.equipo.name)

class TipoPlan(models.Model):
    _name        ='tipoplan.mantenimiento'
    name         = fields.Char(size=25,required=True,string='Nombre')

class GrupoParte(models.Model):
    _name   = 'grupoproceso.mantenimiento'
    name         = fields.Char(size=60,required=True,string='Nombre')
class Proceso(models.Model):
    _name   = 'proceso.mantenimiento'
    name         = fields.Char(size=60,required=True,string='Nombre')
    grupo   = fields.Many2one('grupoproceso.mantenimiento',string="Grupo/Parte")
    plan     = fields.Many2one('plan.mantenimiento')


class EstadoProceso(models.Model):
   _name   = 'estadoproceso.mantenimiento'
   name    = fields.Char(size=5,required=True,string='Nombre')

class TipoTarea(models.Model):
   _name   = 'tipotarea.mantenimiento'
   name         = fields.Char(size=25,required=True,string='Nombre')


class Tarea(models.Model):
    _name       = 'tarea.mantenimiento'
    name        = fields.Char(size=60,required=True,string='Nombre',write=['pmant.group_pmant_admin'])
    tipo        = fields.Many2one('tipotarea.mantenimiento',string="Tipo")
    ubicacion   = fields.Many2one('ubicaciones.mantenimiento',string="Ubicacion")
    planequipo  = fields.One2many('planequipo.mantenimiento', 'tarea',string='Plan / Equipo',required=True)
    clasi1      = fields.Char(size=50,string="Clasificacion 1")
    clasi2      = fields.Char(size=50,string="Clasificacion 2")
    prioridad   = fields.Many2one('prioridad.mantenimiento',string="Prioridad")
    adjunto     = fields.Binary()
    ots         = fields.One2many('maintenance.request','tarea',string="ots")
    ots2        = fields.Many2one('maintenance.request',string="ots2")
    procesos    = fields.One2many('planequipoproceso.mantenimiento', 'tarea' ,string="Estado de  Procesos")
#    procesos    =  fields.Many2many('proceso.mantenimiento',string="Procesos a Utilizar") 

class OTS(models.Model):
    _name       = 'maintenance.request'
    _inherit    = 'maintenance.request'
    tarea       = fields.Many2one('tarea.mantenimiento',string='Tarea')
    procesosr   = fields.One2many('planequipoproceso.mantenimiento', 'tarea' ,related="tarea.procesos" , string="Estado de  Procesos")
    procesos    = fields.One2many('planequipoproceso.mantenimiento','ots' ,string="Estado de  Procesos")
#    e           = fields.Char()
#    @api.depends('id','tarea','procesosr','procesos') 
#    @api.onchange('plan')
#    def _onchange_precio(self):
        #ingresar datos a la clase PLANEQUIPO PROCESO CON LOS PROCESOS DE PLAN ELEGIDO 
#        for record in self.procesos
#            self.e = e + 1
#            record.ots= self.id
class Contacto(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'
    direccion_mantenimiento = fields.Many2many('ubicaciones.mantenimiento',string="Direcciones Mantenimiento")
#'''

#class QrGenerator(models.Model):
 #   _inherit = "maintenance.equipment"
 #   qr_product = fields.Binary('QR Product')
 #   qr_product_name = fields.Char(default="product_qr.png")
 #   def generate_product_sku(self):
 #       qr = qrcode.QRCode(version=1,error_correction=qrcode.constants.ERROR_CORRECT_L,box_size=20,border=4)
 #       name = self.default_code+'_Product.png'
 #       qr.add_data(self.default_code)
 #       qr.make(fit=True)
 #       img = qr.make_image()
 #       buffer = cStringIO.StringIO()
 #       img.save(buffer, format="PNG")
 #       img_str = base64.b64encode(buffer.getvalue())
 #       self.write({'qr_product': img_str,'qr_product_name':name})


# class pmant(models.Model):
#     _name = 'pmant.pmant'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100
